package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.ManagedImage;
import com.ngt.jopenmetaverse.shared.util.FileUtils;

public class ImgHelperTest {

	String resourceLocation;
	String outputLocation;
	@Before
	public void setup() throws Exception
	{
		URL propLoc  =  getClass().getClassLoader().getResource("android_test_resources_dir.properties");

		
		String basePath = org.apache.commons.io.FileUtils.readFileToString(new File(propLoc.getPath())).trim();
		outputLocation = basePath + "output";
		File f = new File(outputLocation);
		if(!f.exists())
			f.mkdir();
		
		resourceLocation = FileUtils.combineFilePath(new File(basePath).getAbsolutePath()
		, "files/images/jpeg2000/");
		System.out.println(resourceLocation);
	}
	
	
	@Test
	public void EncodeFromImageTest() throws Exception
	{
		BitmapFactoryStubImpl bitmapFactory = new BitmapFactoryStubImpl();
		try{
		File[] files = FileUtils.getFileList(resourceLocation, true);
		for(File f: files)
		{
			ImgHelper imgHelper = new ImgHelper();
			FileInputStream fs = new FileInputStream(f);
			IBitmap bitmap = imgHelper.getBitmapForJpeg2000(bitmapFactory, fs);
			
			File outputFile = new File(outputLocation + "/" + f.getName() + ".tga" );
			System.out.println(outputFile);
			FileUtils.writeBytes(outputFile, new ManagedImage(bitmap).ExportTGA());
//			FileOutputStream os =  new FileOutputStream(outputFile); 
//			ImageIO.write( ((BitmapBufferedImageImpl)bitmap).getImage(), "png",  os );
//			bitmap.compress(CompressFormat.PNG, 100, os);
			fs.close();
//			os.close();
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
