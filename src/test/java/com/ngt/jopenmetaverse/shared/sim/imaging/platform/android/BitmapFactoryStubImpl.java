package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmapFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.jclient.BitmapBufferedImageImpl;

public class BitmapFactoryStubImpl implements IBitmapFactory{

	public IBitmap getNewIntance(int w, int h, int[] pixels) {
		return new BitmapBufferedImageImpl(w, h, pixels);
	}
}