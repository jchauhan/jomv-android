package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;

import javax.imageio.ImageIO;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.ManagedImage;
import com.ngt.jopenmetaverse.shared.util.FileUtils;

public class AndroidBitmapBufferedImageImplTest {
	String resourceLocation;
	String outputLocation;
	@Before
	public void setup() throws Exception
	{
		URL propLoc  =  getClass().getClassLoader().getResource("android_test_resources_dir.properties");

		
		String basePath = org.apache.commons.io.FileUtils.readFileToString(new File(propLoc.getPath())).trim();
		outputLocation = basePath + "output";
		File f = new File(outputLocation);
		if(!f.exists())
			f.mkdir();
		
		resourceLocation = FileUtils.combineFilePath(new File(basePath).getAbsolutePath()
		, "files/images/jpg/");
		System.out.println(resourceLocation);
	}
	
	@Test
	public void cloneRotateAndFlipTests() throws Exception
	{
		BitmapFactoryStubImpl bitmapFactory = new BitmapFactoryStubImpl();
		try{
		File[] files = FileUtils.getFileList(resourceLocation, true);
		AndroidBitmapBufferedImageImpl abbi;
		for(File f: files)
		{
			
			Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
			abbi = new AndroidBitmapBufferedImageImpl(bitmap);
			Bitmap resizedbitmap = ((AndroidBitmapBufferedImageImpl)abbi.cloneAndResize(bitmap.getWidth()*5, bitmap.getHeight()*5)).getImage();
			
	        FileOutputStream os = new FileOutputStream(new File(FileUtils.combineFilePath(outputLocation, f.getName() + ".jpeg")));
	        resizedbitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
	        os.flush();
	        os.close();
			
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
