package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;
import com.ngt.jopenmetaverse.shared.sim.ApplicationContextAndroidImpl;


public class ApplicationContextAndroidStubImpl extends ApplicationContextAndroidImpl{
	@Override
	public void init()
	{	
		setBitmapFactory(new BitmapFactoryStubImpl());
		setupTGAImageReaderFactory();
		setupOpenJPEGFactory();
	}
}
