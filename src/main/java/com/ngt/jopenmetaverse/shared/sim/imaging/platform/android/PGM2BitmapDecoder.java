package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmapFactory;

public class PGM2BitmapDecoder {	
	
	public IBitmap decode(IBitmapFactory bitmapFactory, PGMImages pgmImages) throws Exception
	{		
		PPMDecoderLinear[] components = new PPMDecoderLinear[pgmImages.getNumComponents()];

		File[] pgmFiles = pgmImages.getPgmFiles();
		for(int i = 0; i < pgmImages.getNumComponents(); i++)
		{
			components[i] = decode(pgmFiles[i]);
		}

		int w=components[0].getWidth();
		int h=components[0].getHeight();
		
		int[] pixels = new int[w*h];
		int[] argb;
		for(int i =0; i< w*h; i++)
		{
			switch(pgmImages.getNumComponents())
			{
			case 1:
				argb = new int[]{components[0].getLinarRgbPixels()[i], 
						0xff, 0xff, 0xff};
				break;
			case 3:
				argb = new int[]{0xff, 
						components[0].getLinarRgbPixels()[i],
						components[1].getLinarRgbPixels()[i],
						components[2].getLinarRgbPixels()[i]};
				break;
			case 4:
				argb = new int[]{components[3].getLinarRgbPixels()[i], components[0].getLinarRgbPixels()[i],
						components[1].getLinarRgbPixels()[i],
						components[2].getLinarRgbPixels()[i]};
				break;
				default:
					throw new Exception("Invalid number of components: " + pgmImages.getNumComponents() 
							+ " only 1, 3 or 4 components are supported");		
			}
			
			pixels[i] = ((argb[0] & 0xff) << 24) | ((argb[1] & 0xff) << 16)
					| ((argb[2] & 0xff) << 8) | (argb[3] & 0xff);
			
//			System.out.print("<" + argb[0] + " " + pixels[i] + ">");
		}
		
//		System.out.println(String.format("Components %d, Width %d, Height %d, Pixel Size %d", pgmImages.getNumComponents(), w, h, pixels.length));
		
//		Bitmap bm = Bitmap.createBitmap( pixels, w, h, Bitmap.Config.ARGB_8888 );

		
		
		return bitmapFactory.getNewIntance(w, h, pixels);
	}
	
	public PPMDecoderLinear decode( File f ) throws IOException {
		InputStream is = new FileInputStream(f);
		PPMDecoderLinear dec = new PPMDecoderLinear( is );
		dec.readImage();
		is.close();
		return dec;
	}
}
