package com.ngt.jopenmetaverse.shared.sim;

import com.ngt.jopenmetaverse.shared.sim.imaging.BitmapFactory.BitmapFactoryMachine;
import com.ngt.jopenmetaverse.shared.sim.imaging.BitmapFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmapFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.IOpenJPEG;
import com.ngt.jopenmetaverse.shared.sim.imaging.ITGAImageReader;
import com.ngt.jopenmetaverse.shared.sim.imaging.OpenJPEGFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.TGAImageReaderFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.TGAImageReaderFactory.TGAImageReaderMachine;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.android.BitmapFactoryImpl;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.android.OpenJPEGImpl;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.android.TGAImageReaderImpl;


public class ApplicationContextAndroidImpl {
	
	protected IBitmapFactory bitmapFactory;
	
	public IBitmapFactory getBitmapFactory() {
		return bitmapFactory;
	}

	public void setBitmapFactory(IBitmapFactory bitmapFactory) {
		this.bitmapFactory = bitmapFactory;
	}

	public void init()
	{	
		if(bitmapFactory==null) 
			bitmapFactory = new BitmapFactoryImpl();
		setupBitmapFactory();
		setupTGAImageReaderFactory();
		setupOpenJPEGFactory();
	}
	
	protected void setupBitmapFactory()
	{
		BitmapFactoryMachine bitmapFactoryMachine = new BitmapFactoryMachine()
		{
			@Override
			public IBitmapFactory getInstance() {
				return bitmapFactory;
			}
		};
		
		BitmapFactory.setBitmapFactoryMachine(bitmapFactoryMachine);
	}
	
	
	protected void setupTGAImageReaderFactory()
	{
		TGAImageReaderMachine machine = new TGAImageReaderFactory.TGAImageReaderMachine()
		{
			@Override
			public ITGAImageReader createInstance() {
				TGAImageReaderImpl r = new TGAImageReaderImpl();
				r.setBitmapFactory(bitmapFactory);
				return r;
			}
		};
		TGAImageReaderFactory.setTGAImageReaderMachine(machine);
	}
	
	protected void setupOpenJPEGFactory()
	{
		OpenJPEGFactory.OpenJPEGMachine machine = new OpenJPEGFactory.OpenJPEGMachine()
		{
			@Override
			public IOpenJPEG getInstance() {
				IOpenJPEG iOpenJPEG= new OpenJPEGImpl();
				((OpenJPEGImpl)iOpenJPEG).setBitmapFactory(bitmapFactory);
				return iOpenJPEG;
			}
		};
		OpenJPEGFactory.setOpenJPEGMachine(machine);
	}
}
