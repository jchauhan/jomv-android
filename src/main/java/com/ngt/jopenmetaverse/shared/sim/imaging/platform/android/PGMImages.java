package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import java.io.File;

public class PGMImages 
{
	/*
	 * numComponents can be 1, 3, 4, 5
	 * pgmFiles should be in the order BARGB  (Bump, Alpha, Red, Green, Blue)
	 */
	int numComponents;
	File[] pgmFiles;
	
	public PGMImages() {
		super();
	}
	
	public int getNumComponents() {
		return numComponents;
	}
	public void setNumComponents(int numComponents) {
		this.numComponents = numComponents;
	}
	public File[] getPgmFiles() {
		return pgmFiles;
	}
	public void setPgmFiles(File[] pgmFiles) {
		this.pgmFiles = pgmFiles;
	}
}
