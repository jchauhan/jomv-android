package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import android.graphics.Bitmap;

public class PPM2BitmapDecoder {
	private PPMDecoderLinear dec;
	
	public Bitmap decode( File f ) throws IOException {
		InputStream is = new FileInputStream(f);
		dec = new PPMDecoderLinear( is );
		dec.readImage();
		is.close();
		Bitmap bm = Bitmap.createBitmap( dec.getLinarRgbPixels(), dec.getWidth(), dec.getHeight(), Bitmap.Config.RGB_565 );
		return bm;
	}
}
