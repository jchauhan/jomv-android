package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmapFactory;
import com.ngt.jopenmetaverse.shared.util.FileUtils;

import jj2000facade.JJ2000DecoderFacade;
import jj2000facade.JJ2000EncoderFacade;

public class ImgHelper {
	
	File cacheDir = null;

	public ImgHelper() {
		super();
	}
	
	public ImgHelper(File cacheDir) {
		super();
		this.cacheDir = cacheDir;
	}
	
	public IBitmap getBitmapForJpeg2000(IBitmapFactory bitmapFactory, InputStream in ) throws Exception
	{
		File j2000 = copyJpeg2000toTmpFile(in);
		PGMImages pgmImages = decodeJ2000toPGM(j2000);
		IBitmap b = new PGM2BitmapDecoder().decode(bitmapFactory, pgmImages);
		deleteAll(pgmImages.getPgmFiles());
		j2000.delete();
		return b;
	}
	
	public byte[] getJpeg2000ForBitmap(IBitmap b, boolean codeStreamOnly) throws Exception
	{
		PGMImages pgmImages = new Bitmap2PGMEncoder().encode(b, cacheDir);
		File j2000 = encodePGMtoJ2000(pgmImages, codeStreamOnly);		
		byte[] bytes = FileUtils.readBytes(j2000);
		
		deleteAll(pgmImages.getPgmFiles());
		j2000.delete();
		return bytes;
	}
	
	private PGMImages decodeJ2000toPGM( File jp2 ) throws IOException 
	{
		PGMImages pgmImages = new PGMImages(); 
		JJ2000DecoderFacade a = new JJ2000DecoderFacade();
        File tmpBPP = createTempFile("jomv", "tmpBPP.pgm", cacheDir );
        tmpBPP.delete();
        
        a.decode(jp2.getAbsolutePath(), tmpBPP.getAbsolutePath());
        pgmImages.setNumComponents(a.getNumComponents());
        File[] files = new File[a.getNumComponents()];
        String[] outputPaths = a.getOutputFilesPaths();
        for(int i =0; i < outputPaths.length; i++)
        {
        	files[i] = new File(outputPaths[i]);
//            System.out.println("PGM file " + i + " " + outputPaths[i]);
        }
        
        pgmImages.setPgmFiles(files);
        return pgmImages;
	}
	
	private File encodePGMtoJ2000(PGMImages pgmImages, boolean codeStreamOnly) throws IOException
	{
		File outputFile = File.createTempFile("jomv", "tmpJ2000.jp2", cacheDir);
		JJ2000EncoderFacade encoder = new JJ2000EncoderFacade();
		encoder.encode(combineFilePaths(pgmImages.getPgmFiles()), 
				outputFile.getAbsolutePath(), codeStreamOnly);
		return outputFile;
	}
	
	private String combineFilePaths(File[] files)
	{
		String outputPath = "";
		for(int i = 1; i < files.length; i++)
		{		
			outputPath += files[i].getAbsolutePath() + " ";
		}
		outputPath += files[0].getAbsolutePath() + " ";
		outputPath = outputPath.trim().replaceAll(" ", ",");
		return outputPath;
	}
	
	private File copyJpeg2000toTmpFile( InputStream in ) throws IOException
	{
        File tmpJ2000 = createTempFile("jomv", "tmpJ2000.jp2", cacheDir);
        tmpJ2000.createNewFile();
        
        OutputStream os = new FileOutputStream(tmpJ2000);
        
        byte[] data = new byte[in.available()];
        in.read(data);
        os.write(data);
        in.close();
        os.close();
        
        return tmpJ2000;
	}

//	private File decodeJ2000toPPM( File jp2 ) throws IOException 
//	{
//		JJ2000DecoderFacade a = new JJ2000DecoderFacade();
//        File tmpBPP = createTempFile("jomv", "tmpBPP.ppm", cacheDir );
//        tmpBPP.createNewFile();
//        a.decode(jp2.getAbsolutePath(), tmpBPP.getAbsolutePath());
//        
//        return tmpBPP;
//	}

	private File createTempFile(String prefix, String suffix, File tmpDir) throws IOException
	{
		if(tmpDir == null)
			return File.createTempFile(prefix, suffix );
		else
		 return File.createTempFile(prefix, suffix, tmpDir );

	}
	
	private void deleteAll(File[] files)
	{
		for(File f: files)
		{
			if(f.exists())
				f.delete();
		}
	}
	
//	public File copyBPPtoPublicStorage( File bpp) throws IOException {
//		File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//        File file = new File(path, "copy.ppm");
//
//        // Make sure the Pictures directory exists.
//        path.mkdirs();
//
//        InputStream is = new FileInputStream(bpp);
//        OutputStream os = new FileOutputStream(file);
//        byte[] data = new byte[is.available()];
//        is.read(data);
//        os.write(data);
//        is.close();
//        os.close();
//        
//        return file;
//	}
}
