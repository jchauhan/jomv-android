package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import java.io.ByteArrayInputStream;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmapFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.IOpenJPEG;
import com.ngt.jopenmetaverse.shared.sim.imaging.ManagedImage;

public class OpenJPEGImpl implements IOpenJPEG {

	protected IBitmapFactory bitmapFactory;
	
	public IBitmapFactory getBitmapFactory() {
		return bitmapFactory;
	}

	public void setBitmapFactory(IBitmapFactory bitmapFactory) {
		this.bitmapFactory = bitmapFactory;
	}

	public byte[] Encode(ManagedImage image, boolean lossless) throws Exception {
		IBitmap bitmap = bitmapFactory.getNewIntance(image.Width, image.Height, image.ExportPixels());
		return EncodeFromImage(bitmap, lossless);
	}

	public byte[] Encode(ManagedImage image) throws Exception {
		return Encode(image, false);
	}
	
	//TODO Handling Bump .. currntly it supports only ARGB colors
	public IBitmap DecodeToIBitMap(byte[] encoded) throws Exception{
		ByteArrayInputStream bais = new ByteArrayInputStream(encoded);
		ImgHelper imgHelper = new ImgHelper();
		IBitmap b = imgHelper.getBitmapForJpeg2000(bitmapFactory, bais);
		return b;
	}
	
	public DecodeToImageResult DecodeToImage2(byte[] encoded) throws Exception {
		IBitmap img2 = DecodeToIBitMap(encoded);
		return new DecodeToImageResult(new ManagedImage(img2), img2);
	}

	public ManagedImage DecodeToImage(byte[] encoded) throws Exception {
		return DecodeToImage2(encoded).getManagedImage();
	}
//
//	public BufferedImage pngTransformation(BufferedImage bitmap) throws IOException
//	{
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		ImageIO.write( bitmap, "png",  baos );
//		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
//		FileUtils.closeStream(baos);
//		BufferedImage pngbitmap = ImageIO.read( bais );
//		FileUtils.closeStream(bais);
//		return pngbitmap;
//	}
	
	//TODO need to Handle Bump .. currntly it supports only ARGB colors
	public byte[] EncodeFromImage(IBitmap inputbitmap, boolean lossless) throws Exception {
		ImgHelper imgHelper = new ImgHelper();
		byte[] bytes = imgHelper.getJpeg2000ForBitmap(inputbitmap, true);
		return bytes;
	}
}
