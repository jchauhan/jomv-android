package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class Bitmap2PGMEncoder {

	public PGMImages encode(IBitmap bitmap, File cacheDir) throws Exception
	{	
		PGMImages pgmImages = new PGMImages();
		//ARGB by default
		pgmImages.setNumComponents(4);
		File[] files = new File[pgmImages.getNumComponents()]; 
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		for(int i = 0; i< pgmImages.getNumComponents(); i++)
		{
			File f = File.createTempFile("jomv", i + ".pgm", cacheDir);
			System.out.println("Created tmp file:" + f.getAbsolutePath());
			RandomAccessFile raf = new RandomAccessFile(f, "rw");
			files[i] = f;
			writeHeaderInfo(raf, w, h);
			writeComponent(raf, bitmap, w, h, 0xff000000L >> (i*8), (pgmImages.getNumComponents() - 1 - i)*8);
			raf.close();
		}		
		pgmImages.setPgmFiles(files);
		return pgmImages;
		
	}
	
	
	private void writeComponent(RandomAccessFile out, IBitmap bitmap, int w, int h, 
			long mask, int rightshift) throws IOException
	{
		System.out.println("Mask" + Utils.bytesToHexDebugString(Utils.int64ToBytes(mask), "") 
				+ " Rightshift " + rightshift + "Bits at 10 10"
				+ Utils.bytesToHexDebugString(Utils.intToBytes(bitmap.getRGB(10, 10)), ""));
		byte[] bytes = new byte[w*h];
		for(int y = 0; y < h; y++)
		{
			for(int x = 0; x< w; x++)
			{
				bytes[y*w+x] = (byte)((bitmap.getRGB(x, y) & mask) >> rightshift);
//				System.out.println("<" + Utils.bytesToHexDebugString(Utils.int64ToBytes(bitmap.getRGB(x, y)), "") 
//						+ " --> " +  Utils.bytesToHexDebugString(new byte[] {bytes[y*w+x]}, ""));
			}
		}
		
		out.write(bytes);
	}
	
	 /**
     * Writes the header info of the PGM file :
     *
     * P5
     * width height
     * 255
     *
     * @exception IOException If there is an IOException
     * */
     private int writeHeaderInfo(RandomAccessFile  out, int w, int h) throws IOException{
        byte[] byteVals;
        int i;
        String val;
        int offset;
        
        // write 'P5' to file
        out.writeByte('P'); // 'P'
        out.write('5'); // '5'
        out.write('\n'); // newline
        offset=3;
        // Write width in ASCII
        val=String.valueOf(w);
        byteVals=val.getBytes();
        for(i=0;i<byteVals.length;i++){
            out.writeByte(byteVals[i]);
            offset++;
        }
        out.write(' '); // blank
        offset++;
        // Write height in ASCII
        val=String.valueOf(h);
        byteVals=val.getBytes();
        for(i=0;i<byteVals.length;i++) {
            out.writeByte(byteVals[i]);
            offset++;
        }
        // Write maxval
        out.write('\n'); // newline
        out.write('2'); // '2'
        out.write('5'); // '5'
        out.write('5'); // '5'
        out.write('\n'); // newline
        offset+=5;
        
        return offset;
     }
}
