package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmapFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.PixelFormat;

public class BitmapFactoryImpl implements IBitmapFactory{

	public IBitmap getNewIntance(int w, int h, int[] pixels) {
		return new AndroidBitmapBufferedImageImpl(w, h, pixels);
	}

	public IBitmap getNewIntance(int w, int h, PixelFormat format) {
		return new AndroidBitmapBufferedImageImpl(w, h, format);
	}
}
