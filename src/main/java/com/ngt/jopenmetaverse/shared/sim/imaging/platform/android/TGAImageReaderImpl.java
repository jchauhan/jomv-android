package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import java.io.InputStream;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmapFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.ITGAImageReader;
import com.ngt.jopenmetaverse.shared.sim.imaging.platform.android.tga.AndroidTGADecoder;
import com.ngt.jopenmetaverse.shared.util.JLogger;
import com.ngt.jopenmetaverse.shared.util.Utils;

public class TGAImageReaderImpl implements ITGAImageReader{
	
	private IBitmapFactory bitmapFactory = new BitmapFactoryImpl(); 

	public IBitmapFactory getBitmapFactory() {
		return bitmapFactory;
	}

	public void setBitmapFactory(IBitmapFactory bitmapFactory) {
		this.bitmapFactory = bitmapFactory;
	}

	public IBitmap read(InputStream stream) {
		try {			
			IBitmap img2 = (AndroidTGADecoder.loadImage(bitmapFactory, stream));
			return img2;
			} catch (Exception e) {
				JLogger.warn(Utils.getExceptionStackTraceAsString(e));
				return null;
			}
	}	
}
