package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import com.ngt.jopenmetaverse.shared.exception.NotSupportedException;
import com.ngt.jopenmetaverse.shared.sim.imaging.AbstractIBitmapImpl;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.PixelFormat;

public class AndroidBitmapBufferedImageImpl  extends AbstractIBitmapImpl
{
	Bitmap img;
	static Map<PixelFormat, Bitmap.Config> PixelFormatMap = new HashMap<PixelFormat, Bitmap.Config>();
	static Map<Bitmap.Config, PixelFormat> PixelFormatMapReverse = new HashMap<Bitmap.Config, PixelFormat>();
	static 
	{
		PixelFormatMap.put(PixelFormat.Format32bppArgb, Bitmap.Config.ARGB_8888);
		PixelFormatMap.put(PixelFormat.Format24bppRgb, Bitmap.Config.ARGB_8888);
		PixelFormatMap.put(PixelFormat.Format32bppRgb, Bitmap.Config.ARGB_8888);
		//TODO Need to verify 
		PixelFormatMap.put(PixelFormat.Format16bppGrayScale, Bitmap.Config.ALPHA_8);
		PixelFormatMap.put(PixelFormat.Format8bppGrayScale, Bitmap.Config.ALPHA_8);
		PixelFormatMap.put(PixelFormat.Custom, Bitmap.Config.ARGB_8888);

		
		//Add here more mapping
		
		for(Entry<PixelFormat, Bitmap.Config> e: PixelFormatMap.entrySet())
		{
			PixelFormatMapReverse.put(e.getValue(), e.getKey());
		}
	}
	
	public AndroidBitmapBufferedImageImpl(int width, int height, Bitmap.Config imageType) {
		img = Bitmap.createBitmap(width, height, imageType);
	}
	
	public AndroidBitmapBufferedImageImpl(int width, int height, PixelFormat format) {
		img = Bitmap.createBitmap(width, height, PixelFormatMap.get(format));
	}
	
	public AndroidBitmapBufferedImageImpl(Bitmap img) {
		this.img = img;
	}
	
	public AndroidBitmapBufferedImageImpl(int w, int h, int[] pixels) {
		img = Bitmap.createBitmap(pixels,  w, h, Bitmap.Config.ARGB_8888 );
	}
	
	public boolean hasPixelFormat(PixelFormat pixelFormat) throws NotSupportedException{
		if(!PixelFormatMap.containsKey(pixelFormat))
			return false;
		
		return img.getConfig() == PixelFormatMap.get(pixelFormat); 
	}
	
	
	 public int getRGB(int x, int y)
	{
		return img.getPixel(x, y);
	}

	 public void setRGB(int x, int y, int rgb)
	{
		img.setPixel(x, y, rgb);
	}
	 
	public String getPixelFormatAsString() {
		if(PixelFormatMapReverse.containsKey(img.getConfig()))
			return PixelFormatMapReverse.get(img.getConfig()).toString();
		else
			return String.format("Unknown Format with int value (%d)", img.getConfig());  
			
	}

	public PixelFormat getPixelFormat() {
		return PixelFormatMapReverse.get(img.getConfig());
	}

	public int getHeight() {
		return img.getHeight();
	}

	public int getWidth() {
		return img.getWidth();
	}
	
	public Bitmap getImage()
	{
		return img;
	}

	public void setImage(Bitmap img)
	{
		this.img = img;
	}

	public IBitmap createIBitmap(int w, int h, int[] pixels) {
		return new AndroidBitmapBufferedImageImpl(h, w, pixels);
	}

	public void resize(int w, int h) {
		AndroidBitmapBufferedImageImpl bimg = (AndroidBitmapBufferedImageImpl)cloneAndResize(w, h);
		img.recycle();
		img = bimg.getImage();
	}

	public void tile(int tiles) {
		AndroidBitmapBufferedImageImpl bimg = (AndroidBitmapBufferedImageImpl)cloneAndTile(tiles);
		img.recycle();
		img = bimg.getImage();		
	}

	public void rotateAndFlip(double radians, boolean flipx, boolean flipy) {
		AndroidBitmapBufferedImageImpl bimg = (AndroidBitmapBufferedImageImpl)cloneRotateAndFlip(radians, flipx, flipy);
		img.recycle();
		img = bimg.getImage();		
	}

	public IBitmap cloneAndTile(int tiles) {
		Bitmap wideBmp =  Bitmap.createBitmap(img.getWidth() * tiles, 
		        img.getHeight()*tiles, img.getConfig());
		 Canvas wideBmpCanvas = new Canvas(wideBmp); 
		 Rect src =  new Rect(0, 0, img.getWidth(), img.getHeight());
		 Rect dst;
		 for (int x = 0; x < tiles; x++)
         {
             for (int y = 0; y < tiles; y++)
             {
            	 dst = new Rect(x * 256, y * 256, x * 256 + 256, y * 256 + 256);
            	 wideBmpCanvas.drawBitmap(img, src, dst, null);
             }
         }
		return new AndroidBitmapBufferedImageImpl(wideBmp);
	}

	public IBitmap cloneAndResize(int w, int h) {
		Bitmap newBitmap = Bitmap.createScaledBitmap(img, w, h, true);
 		return new AndroidBitmapBufferedImageImpl(newBitmap);
	}

	public IBitmap cloneRotateAndFlip(double radians, boolean flipx,
			boolean flipy) {
		Bitmap wideBmp =  Bitmap.createBitmap(img.getWidth(), 
		        img.getHeight(), img.getConfig());
		 Canvas wideBmpCanvas = new Canvas(wideBmp); 
		 Matrix m = new Matrix();
		 m.postScale(flipx ? -1 : 1, flipy ? -1 : 1);
		 m.postTranslate(flipx? -1* this.getWidth() : 0, flipy? -1*this.getHeight() : 0);
		 m.postRotate((float)Math.toDegrees(radians), this.getWidth()/2, this.getHeight()/2 );
    	 wideBmpCanvas.drawBitmap(img, m, null);
 		return new AndroidBitmapBufferedImageImpl(wideBmp);
	}
	
	public IBitmap createImageWithSolidColor(int width, int height, int r, int g, int b, int a)
	{
		Bitmap wideBmp =  Bitmap.createBitmap(width, 
		        height, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(wideBmp);
		// get the int for the colour which needs to be removed
		Paint p = new Paint();
		p.setARGB(a, r, g, b); // ARGB for the color
		c.drawPaint(p);
 		return new AndroidBitmapBufferedImageImpl(wideBmp);
	}
	
	public void dumpToFile(String filePath) throws Exception
	{
		FileOutputStream fos = new FileOutputStream(new File(filePath));
        img.compress(Bitmap.CompressFormat.JPEG, 100, fos);
		fos.flush();
		fos.close();
	}
	
	public void dispose() {
		img.recycle();
	}
}
