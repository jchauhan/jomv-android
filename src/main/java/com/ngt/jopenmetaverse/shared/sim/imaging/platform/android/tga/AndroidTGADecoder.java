package com.ngt.jopenmetaverse.shared.sim.imaging.platform.android.tga;

import java.io.InputStream;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmap;
import com.ngt.jopenmetaverse.shared.sim.imaging.IBitmapFactory;
import com.ngt.jopenmetaverse.shared.sim.imaging.tga.CoreTGADecoder;

public class AndroidTGADecoder extends CoreTGADecoder{

	public static IBitmap loadImage(IBitmapFactory bitmapFactory, InputStream source) throws Exception
	{
		DecodedImage decodedImage = decodeImage(source);
		TgaHeader header = decodedImage.getHeader();
		byte[] decoded = decodedImage.getPixels();

		int[] pixels = new int[header.ImageSpec.Height * header.ImageSpec.Width];
		
//		Bitmap b = null;
		//			System.out.println("\n\nStoring Pixels");

		if (header.ImageSpec.getAlphaBits() > 0 ||
				header.ImageSpec.PixelDepth == 8 ||	// Assume  8 bit images are alpha only
				header.ImageSpec.PixelDepth == 32)	// Assume 32 bit images are ARGB
		{
//			b = Bitmap.createBitmap(
//					header.ImageSpec.Width,
//					header.ImageSpec.Height,
//					Bitmap.Config.ARGB_8888);

			for(int j = 0; j < header.ImageSpec.Height; j++)
				for(int i = 0; i < header.ImageSpec.Width; i++) {
					int index = ( j* header.ImageSpec.Width + i) * (4);

					int value =  ((decoded[index + 3] & 0xFF) << 0) |
							((decoded[index + 2] & 0xFF) <<  8) |
							((decoded[index + 1] & 0xFF) << 16) | ((decoded[index + 0] & 0xFF) << 24);

					//						if(value != 0)
					//							System.out.println(String.format("<X = %d,  Y = %d, %d %s -- %s>", i, j, index, 
					//									Utils.bytesToHexDebugString(Utils.intToBytes(value), ""),
					//									Utils.bytesToHexDebugString(Arrays.copyOfRange(decoded, index, index + 4), "")
					//									));

//					b.setPixel(i, j,value);
					pixels[j*header.ImageSpec.Width + i] = value;
				} 
		}
		else
		{
//			b = Bitmap.createBitmap(
//					header.ImageSpec.Width,
//					header.ImageSpec.Height,
//					Bitmap.Config.ARGB_8888);
			for(int j = 0; j < header.ImageSpec.Height; j++)
				for(int i = 0; i < header.ImageSpec.Width; i++) {
					int index = ( (j)* header.ImageSpec.Width + i) * (4);

					//  					System.out.println(String.format("rawdata = %d height = %d width = %d j = %d i = %d index = %d", 
					//  							rawData.length, height, width, j, i , index));
					int value =  ((decoded[index + 3] & 0xFF) << 0)|
							((decoded[index + 2] & 0xFF) <<  8)|
							((decoded[index + 1] & 0xFF) << 16) | ((0xff) << 24);
					//						if(value != 0)
					//						{
					//							System.out.print(Utils.bytesToHexDebugString(Utils.intToBytes(value), "Assigned ARGB"));
					//						}

//					b.setPixel(i, j,value);
					pixels[j*header.ImageSpec.Width + i] = value;
				}  
		}
		return bitmapFactory.getNewIntance(header.ImageSpec.Width, header.ImageSpec.Height, pixels);
	}
}
